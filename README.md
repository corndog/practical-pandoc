## Practical Pandoc

Short, easy-to-inspect [pandoc] templates implementing suggestions from [Butterick's
Practical Typography][practicaltype] that are ready to use *right now* or build on top of.
Currently there is only a resume template.

### Requirements

* [pandoc]
* [lualatex][luatex], comes in modern [texlive] packages

### Usage

To get started right away, you'll want to be familiar with [pandoc's flavor of
Markdown][pandoc-markdown]. Pandoc itself supports [other variants][pandoc-variants], but
this project may not.

A small Makefile is provided to abridge the calls to luatex and the template.

```sh
# pandoc --pdf-engine=lualatex --template=./templates/resume.tex -f markdown resume.md -o ./out/resume.pdf
make resume
# pandoc --pdf-engine=lualatex --template=./templates/resume.tex -f markdown ./example.md -o ~/documents/resume.pdf
make resume EXT=+lists_without_preceding_blankline IN=./example.md OUT=~/documents/resume.pdf
# pandoc --pdf-engine=lualatex --template=./templates/resume.tex -f markdown ./example.md -o ~/documents/example.pdf
make resume EXT=+lists_without_preceding_blankline IN=./example.md OUTDIR=~/documents
# pandoc --pdf-engine=lualatex --template=./templates/resume.tex -f markdown ./resume.md -t latex
make resume TO=latex
```

### Todo

- [ ] DRY Lua multiplication in resume.tex
- [ ] Include CSS to better support resume HTML output
- [ ] Create an anonymizing filter for easy feedback submission
- [ ] Create accompanying letter template

### Issues

There's nothing especially elaborate about these templates. If you want to make changes
for your own purposes, you'll be better served consulting the relevant communities on
[StackExchange][stackexchange] or elsewhere.

### Motivation

I was redoing my résumé design and wanted to use multiple fonts as well as scale spacings
to point size. I was already using pandoc and then I found luatex. Using pandoc helps
separate content from formatting while luatex further simplifies templating, overall
making subsequent rewrites easier. This is the fruit of that effort. Since the basic
design is simple enough (subtle changes go a long way), I decided to share.

My goals are to support different types of everyday and not-so-everyday documents as time
goes on (read: as my need for them arises). Particularly, templates should neither be too
opinionated in their use or in their resulting formatting allowing someone with the
requisite know-how to readily write sensible, if not natural, Markdown or make minor style
adjustments.

[luatex]: http://www.luatex.org/
[pandoc]: https://pandoc.org/
[pandoc-markdown]: http://pandoc.org/MANUAL.html#pandocs-markdown
[pandoc-variants]: http://pandoc.org/MANUAL.html#markdown-variants
[practicaltype]: https://practicaltypography.com/
[stackexchange]: https://stackexchange.com/
[texlive]: https://www.tug.org/texlive/
