OUTDIR=./out
TEMPLATES_DIR=./templates
RESUME_TEMPLATE=$(TEMPLATES_DIR)/resume.tex

.PHONY: clean makeout

clean:
	rm -r $(OUTDIR)

makeout:
	mkdir -p $(OUTDIR)

resume: IN ?= resume.md
resume: OUT ?= $(OUTDIR)/$(basename $(IN)).pdf
	ifeq ($(TO),)
		TARGET = -o $(OUT)
	else
		TARGET = -t $(TO)
	endif
resume: makeout
	pandoc --pdf-engine=lualatex $(FILTERS) --template=$(RESUME_TEMPLATE) -f markdown$(EXT) $(IN) $(TARGET)
