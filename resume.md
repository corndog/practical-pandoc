# Foo Bar
123 Candy Cane Ln, North Pole\
*Phone:* (555) 555-5555 \ \ *Email:* [foobar@example.com][email] \ \ *Portfolio:* [example.com][portfolio]

## Qualifications

I hold the world record in presents shipped worldwide. I programmed and independently
launched an undetectable satellite monitoring behavior.

[^comment]: Make comments with unreferenced footnotes.

abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz

## Education
Ph.D. Giftology (Honorary, 2014)\
North Pole Institute of Technology---Cambridge\
Dissertation in reindeer husbandry\
Taught courses in sleigh riding and factory management\
\
GED\
Completed requirements in 2010.

## Experience
* Senior Partner, North Pole Bank, May 2015--now
  - Went from #2 to #1 performing national bank under my guidance.
  - Increased profit margins by \infty, from $0 to 0.01\textcent.

* Chief Munitions Officer, North Pole Militia, Sept 2009--Oct 2009
  - Trained elves in operations research to make snowballs under budget.
  - Was literally so good at wiping all opposition I wiped out my job.

* Freelance Writer, Feb 2003--Jul 2009
  - Under pen name, published book on random numbers.
  - Ghost writer for ten North Pole Best Sellers.
  - Contributed to [Wikipedia][wikipedia] in native language.

## Skills
### Programming languages
Fortran, COBOL, Prolog, C\texttt{++}14, Perl, Lisp, Racket, Haskell, Elixir, Elm, Casio BASIC, R, Objective C, Smalltalk, Swift, Visual Basic

### Libraries
Qt, Cocoa, Node.js, React, Vue, AngularJS, Angular2, Laravel, Shopify

### Tools
Docker, Kubernetes, PostgreSQL, Travis, AWS, Azure, GCE, Git, SVN, \LaTeX

### Operating Systems
GNU/Linux, MS-DOS, OpenBSD, Plan 9

## Interests
* Hanging out in shopping malls annually
* Don't give to charity but I inspire others to be more charitable
* Long walks on the beach at sunset

[email]: mailto:foobar@example.com
[portfolio]: https://example.com
[wikipedia]: https://wikipedia.com
